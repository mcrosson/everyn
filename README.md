Overview
=
Every N is an app I put together to track an event that needs to happen no more often than every N hours/minutes/seconds.  The app has a countdown timer as well as a stop watch to easily track when the event should occur.  It also includes a persistent notification that shows the countdown and stop watch values.

Note: This is my first real Android app and it's not 100% and was something of a one-off project.  I've disabled the wiki and issue tracker for these reasons.

Known Issues
=
* Using back to leave the app when running will stop the service
* The service doesn't always respond to the notification buttons

Download / Install
=
* fdroid repo: https://nusku.net/fdroid
* apk: https://nusku.net/fdroid/net.nusku.everyn_1.apk

Licensing
=
All code is licensed under the Apache 2.0 license unless otherwise indicated.

Icons were found via iconfinder.com and created by Visual Pharm.  The icons are licensed by Visual Pharm under the  Creative Commons Attribution-No Derivative Works 3.0 Unported (http://creativecommons.org/licenses/by-nd/3.0/) license.
