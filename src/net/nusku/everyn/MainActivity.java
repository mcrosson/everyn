/*
Copyright 2013 Michael Crosson

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package net.nusku.everyn;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

public class MainActivity extends Activity implements ServiceConnection {

	private TextView timerStopwatch;
	private TextView timerCountdown;
	private Button startButton;
	private Button resetButton;
	NumberPicker np_hours;
	NumberPicker np_minutes;
	NumberPicker np_seconds;
	boolean smallFont = false;

	boolean mIsBound;
	private Messenger mServiceMessenger = null;
	private final Messenger mMessenger = new Messenger(
			new IncomingMessageHandler());
	private ServiceConnection mConnection = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		timerStopwatch = (TextView) findViewById(R.id.timerStopwatch);
		timerCountdown = (TextView) findViewById(R.id.timerCountdown);

		np_hours = (NumberPicker) findViewById(R.id.np_hours);
		np_hours.setMaxValue(24);
		np_hours.setMinValue(0);
		np_hours.setWrapSelectorWheel(false);
		np_hours.setValue(1); // FIXME: Make preference

		np_minutes = (NumberPicker) findViewById(R.id.np_minutes);
		np_minutes.setMaxValue(60);
		np_minutes.setMinValue(0);
		np_minutes.setWrapSelectorWheel(false);
		np_minutes.setValue(30); // FIXME: Make preference

		np_seconds = (NumberPicker) findViewById(R.id.np_seconds);
		np_seconds.setMaxValue(60);
		np_seconds.setMinValue(0);
		np_seconds.setWrapSelectorWheel(false);

		startButton = (Button) findViewById(R.id.btnStartReset);
		startButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				stop();
				start();
			}
		});

		resetButton = (Button) findViewById(R.id.btnStop);
		resetButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				stop();
			}
		});

		// Ensure service is auto-started
		if (!MainService.isRunning()) {
			startService(new Intent(MainActivity.this, MainService.class));
			doBindService();
		}

		// Auto-bind service if we are re-entering app / similar
		automaticBind();
	}

	@Override
	public boolean onMenuItemSelected(final int featureId, final MenuItem item) {
		return onOptionsItemSelected(item);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("NP_VALUES_HOURS", np_hours.getValue());
		outState.putInt("NP_VALUES_MINUTES", np_minutes.getValue());
		outState.putInt("NP_VALUES_SECONDS", np_seconds.getValue());
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			np_hours.setValue(savedInstanceState.getInt("NP_VALUES_HOURS"));
			np_minutes.setValue(savedInstanceState.getInt("NP_VALUES_MINUTES"));
			np_seconds.setValue(savedInstanceState.getInt("NP_VALUES_SECONDS"));
		}
		super.onRestoreInstanceState(savedInstanceState);
	}

	private void automaticBind() {
		if (MainService.isRunning()) {
			doBindService();
		}
	}

	private void doBindService() {
		bindService(new Intent(this, MainService.class), mConnection,
				Context.BIND_AUTO_CREATE);
		mIsBound = true;
	}

	private void doUnbindService() {
		if (mIsBound) {
			// If we have received the service, and hence registered with it,
			// then now is the time to unregister.
			if (mServiceMessenger != null) {
				try {
					Message msg = Message.obtain(null,
							MainService.MSG_UNREGISTER_CLIENT);
					msg.replyTo = mMessenger;
					mServiceMessenger.send(msg);
				} catch (RemoteException e) {
					// There is nothing special we need to do if the service has
					// crashed.
				}
			}
			// Detach our existing connection.
			unbindService(mConnection);
			mIsBound = false;
		}
	}

	@Override
	public void onServiceConnected(ComponentName name, IBinder service) {
		mServiceMessenger = new Messenger(service);
		try {
			Message msg = Message.obtain(null, MainService.MSG_REGISTER_CLIENT);
			msg.replyTo = mMessenger;
			mServiceMessenger.send(msg);
		} catch (RemoteException e) {
			// In this case the service has crashed before we could even do
			// anything with it
		}
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
		// This is called when the connection with the service has been
		// unexpectedly disconnected - process crashed.
		mServiceMessenger = null;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			stopService(new Intent(MainActivity.this, MainService.class));
			doUnbindService();
		} catch (Throwable t) {
		}
	}

	private void start() {
		if (mIsBound) {
			smallFont = false;
			timerCountdown.setTextSize(60);
			Bundle toSend = new Bundle();
			toSend.putInt("NP_HOURS", np_hours.getValue());
			toSend.putInt("NP_MINUTES", np_minutes.getValue());
			toSend.putInt("NP_SECONDS", np_seconds.getValue());
			sendMessageToService(MainService.MSG_START, toSend);
		}
	}

	private void stop() {
		sendMessageToService(MainService.MSG_STOP, null);
	}

	private void sendMessageToService(int message, Bundle values) {
		if (mIsBound) {
			if (mServiceMessenger != null) {
				try {
					Message msg = Message.obtain(null, message);
					msg.setData(values);
					msg.replyTo = mMessenger;
					mServiceMessenger.send(msg);
				} catch (RemoteException e) {
				}
			}
		}
	}

	private class IncomingMessageHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			Bundle values = msg.getData();

			switch (msg.what) {
			case MainService.MSG_SET_STOPWATCH_VALUE:
				if (values != null) {
					String stopwatchText = values.getString("STOPWATCH");
					timerStopwatch.setText(stopwatchText);
				}
				break;
			case MainService.MSG_SET_TIMER_VALUE:
				if (values != null) {
					smallFont = values.getBoolean("SMALL_FONT");
					if (smallFont) {
						timerCountdown.setTextSize(30);
					}

					String timerText = values.getString("TIMER");
					timerCountdown.setText(timerText);
				}
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}
}
