/*
Copyright 2013 Michael Crosson

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package net.nusku.everyn;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;

public class MainService extends IntentService {

	public static final int MSG_REGISTER_CLIENT = 1;
	public static final int MSG_UNREGISTER_CLIENT = 2;
	public static final int MSG_SET_STOPWATCH_VALUE = 3;
	public static final int MSG_SET_TIMER_VALUE = 4;
	public static final int MSG_SET_NP_VALUES = 8;
	public static final int MSG_START = 11;
	public static final int MSG_STOP = 12;
	public static final int MSG_RESET = 13;

	private final Messenger mMessenger = new Messenger(
			new IncomingMessageHandler());
	private List<Messenger> mClients = new ArrayList<Messenger>();

	private static boolean isRunning = false;

	NotificationManager mNotificationManager;
	private CountDownTimer countDownTimer;
	private boolean countdownComplete;

	private long startTime = 0L;
	long timeInMillies = 0L;

	int npHours = 0;
	int npMinutes = 0;
	int npSeconds = 0;

	public MainService(String name) {
		super(name);
	}

	public MainService() {
		super("EveryN");
	}

	@Override
	public void onCreate() {
		super.onCreate();
		isRunning = true;
		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mMessenger.getBinder();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		stop();
		isRunning = false;
	}

	public static boolean isRunning() {
		return isRunning;
	}

	public void start() {
		countdownComplete = false;

		startTime = SystemClock.uptimeMillis();

		countDownTimer = new LocalCountdownTimer(npHours * 3600000 + npMinutes
				* 60000 + npSeconds * 1000, 1000 /* Tick every 1s */);
		countDownTimer.start();

		AlarmManager mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		Intent mAlarmManagerIntent = new Intent(this, MainService.class);
		mAlarmManagerIntent.setAction("STOPWATCH_SEC"); // This isn't working,
														// so use extra's as
														// well; Both handled in
														// "onHandleIntent"
		mAlarmManagerIntent.putExtra("ACTION", "STOPWATCH_SEC");
		PendingIntent mAlarmManagerPIntent = PendingIntent.getService(this, 0,
				mAlarmManagerIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		mAlarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
				SystemClock.elapsedRealtime(), 1000, mAlarmManagerPIntent);
	}

	public void stop() {
		countdownComplete = true;
		if (countDownTimer != null) {
			countDownTimer.cancel();
		}

		AlarmManager mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		Intent mAlarmManagerIntent = new Intent(this, MainService.class);
		// These intent properties need to be the same used when starting the
		// alarm manager or they won't cancel properly
		mAlarmManagerIntent.setAction("STOPWATCH_SEC"); // This isn't working,
														// so use extra's as
														// well; Both handled
														// in
														// "onHandleIntent"
		mAlarmManagerIntent.putExtra("ACTION", "STOPWATCH_SEC");
		PendingIntent mAlarmManagerPIntent = PendingIntent.getService(this, 0,
				mAlarmManagerIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		mAlarmManager.cancel(mAlarmManagerPIntent);

		stopForeground(true);

		Bundle toSend = new Bundle();
		toSend.putString("STOPWATCH", "00:00:00");
		toSend.putString("TIMER", "00:00:00");
		sendMessageToUI(MSG_SET_STOPWATCH_VALUE, toSend);
		sendMessageToUI(MSG_SET_TIMER_VALUE, toSend);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		String action = intent.getStringExtra("action");
		if ("STOP".equalsIgnoreCase(action)) {
			stop();
		} else if ("RESET".equalsIgnoreCase(action)) {
			stop();
			start();
		} else if ("STOPWATCH_SEC".equalsIgnoreCase(action)) {
			updateStopWatch();
		} else {
			if (action == null) {
				Bundle extras = intent.getExtras();
				if (extras != null
						&& "STOPWATCH_SEC".equalsIgnoreCase(extras
								.getString("ACTION"))) {
					updateStopWatch();
				}
			}
		}
	}

	private class IncomingMessageHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			Bundle values = msg.getData();

			switch (msg.what) {
			case MSG_REGISTER_CLIENT:
				mClients.add(msg.replyTo);
				break;
			case MSG_UNREGISTER_CLIENT:
				mClients.remove(msg.replyTo);
				break;
			case MSG_SET_NP_VALUES:
				npHours = values.getInt("NP_HOURS");
				npMinutes = values.getInt("NP_MINUTES");
				npSeconds = values.getInt("NP_SECONDS");
				break;
			case MSG_START:
				if (values != null) {
					npHours = values.getInt("NP_HOURS");
					npMinutes = values.getInt("NP_MINUTES");
					npSeconds = values.getInt("NP_SECONDS");
				}
				start();
				break;
			case MSG_STOP:
				stop();
				break;
			case MSG_RESET:
				stop();
				start();
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

	private void sendMessageToUI(int message, Bundle values) {
		Iterator<Messenger> messengerIterator = mClients.iterator();
		while (messengerIterator.hasNext()) {
			Messenger messenger = messengerIterator.next();
			try {
				Message msg = Message.obtain(null, message);
				msg.setData(values);
				messenger.send(msg);
			} catch (RemoteException e) {
				// The client is dead. Remove it from the list.
				mClients.remove(messenger);
			}
		}
	}

	public void processNotification(long countdownMillis) {
		String timeCountdown = "00:00:00";
		String timeStopwatch = "00:00:00";

		timeInMillies = SystemClock.uptimeMillis() - startTime;

		long seconds = timeInMillies / 1000;
		long minutes = seconds / 60;
		seconds = seconds % 60;
		long hours = minutes / 60;
		minutes = minutes % 60;

		timeStopwatch = String.format("%02d", hours) + ":"
				+ String.format("%02d", minutes) + ":"
				+ String.format("%02d", seconds);

		if (countdownMillis > 0) {
			seconds = countdownMillis / 1000;
			minutes = seconds / 60;
			seconds = seconds % 60;
			hours = minutes / 60;
			minutes = minutes % 60;

			timeCountdown = String.format("%02d", hours) + ":"
					+ String.format("%02d", minutes) + ":"
					+ String.format("%02d", seconds);
		}

		// Pick an appropriate status icon
		int idIcon = R.drawable.ic_notready;
		if (countdownComplete) {
			idIcon = R.drawable.ic_ready;
		}
		
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this).setSmallIcon(idIcon)
				.setContentTitle("Every N").setOngoing(true)
				.setProgress(0, 0, true);

		Intent mainActivityIntent = new Intent(getApplicationContext(),
				MainActivity.class);
		PendingIntent mainActivityPIntent = PendingIntent.getActivity(
				getApplicationContext(), 0, mainActivityIntent, 0);
		mBuilder.setContentIntent(mainActivityPIntent);

		NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
		inboxStyle.addLine("Countdown: " + timeCountdown);
		inboxStyle.addLine("Stopwatch: " + timeStopwatch);
		mBuilder.setStyle(inboxStyle);

		Intent resetIntent = new Intent(this, MainService.class);
		resetIntent.putExtra("action", "RESET");
		PendingIntent resetPIntent = PendingIntent.getService(this, 0,
				resetIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		mBuilder.addAction(R.drawable.ic_reset, "Reset", resetPIntent);

		Intent stopIntent = new Intent(this, MainService.class);
		stopIntent.putExtra("action", "STOP");
		PendingIntent stopPIntent = PendingIntent.getService(this, 1,
				stopIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		mBuilder.addAction(R.drawable.ic_stop, "Stop", stopPIntent);

		startForeground(1, mBuilder.build());
	}

	private void updateStopWatch() {
		timeInMillies = SystemClock.uptimeMillis() - startTime;

		long seconds = timeInMillies / 1000;
		long minutes = seconds / 60;
		seconds = seconds % 60;
		long hours = minutes / 60;
		minutes = minutes % 60;

		String updatedStopwatchText = "" + String.format("%02d", hours) + ":"
				+ String.format("%02d", minutes) + ":"
				+ String.format("%02d", seconds);

		Bundle toSend = new Bundle();
		toSend.putString("STOPWATCH", updatedStopwatchText);
		sendMessageToUI(MSG_SET_STOPWATCH_VALUE, toSend);

		if (countdownComplete) {
			processNotification(0);
		}
	}

	private class LocalCountdownTimer extends CountDownTimer {
		public LocalCountdownTimer(long startTime, long interval) {
			super(startTime, interval);
		}

		@Override
		public void onFinish() {
			countdownComplete = true;
			processNotification(0);
			
			String updatedTimerText = "Countdown finished! Please reset to start new interval";

			Bundle toSend = new Bundle();
			toSend.putString("TIMER", updatedTimerText);
			toSend.putBoolean("SMALL_FONT", Boolean.valueOf(true));
			sendMessageToUI(MSG_SET_TIMER_VALUE, toSend);
		}

		@Override
		public void onTick(long millisUntilFinished) {
			long seconds = millisUntilFinished / 1000;
			long minutes = seconds / 60;
			seconds = seconds % 60;
			long hours = minutes / 60;
			minutes = minutes % 60;

			String updatedTimerText = "" + String.format("%02d", hours) + ":"
					+ String.format("%02d", minutes) + ":"
					+ String.format("%02d", seconds);

			Bundle toSend = new Bundle();
			toSend.putString("TIMER", updatedTimerText);
			sendMessageToUI(MSG_SET_TIMER_VALUE, toSend);

			if (!countdownComplete) {
				processNotification(millisUntilFinished);
			}
		}
	}
}
